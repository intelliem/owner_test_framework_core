package com.epam.browser;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChromeBrowser {

    private static WebDriverWait webDriverWait;
    public static WebDriver driver = new ChromeDriver();

    public ChromeBrowser(){
        webDriverWait = new WebDriverWait(driver, 30);
    }

    public static void openDefaultPage() {

    }

    public static void closeBrowser(){
        driver.close();
    }

    public static void openPage(String url){
        driver.get(url);
    }

    public static String getPageTitle(){
        return driver.getTitle();
    }

    public static List<WebElement> findInputFieldsByNameLocator(List<String> fieldLocators){
        List<WebElement> resultFields = new ArrayList<>();

        for (String fieldLocator: fieldLocators) {
            WebElement nameElement = driver.findElement(By.name(fieldLocator));
            resultFields.add(nameElement);
        }

        return resultFields;
    }

    public static List<WebElement> findInputFieldsByNameLocator(String fieldLocator){
        return driver.findElements(By.name(fieldLocator));
    }

    public static WebElement findInputFieldByNameLocator(String fieldLocator){
        return driver.findElement(By.name(fieldLocator));
    }

    public static WebElement findInputFieldByIdLocator(String fieldLocator){
        return driver.findElement(By.id(fieldLocator));
    }

    public static List<WebElement> findInputFieldByXPath(String fieldLocator){
        return driver.findElements(By.xpath(fieldLocator));
    }


}

