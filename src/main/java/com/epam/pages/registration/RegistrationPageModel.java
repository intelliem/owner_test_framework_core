package com.epam.pages.registration;

import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.List;

public class RegistrationPageModel {

    public static final String PAGE_TITLE = "Регистрация пользователя";

    public static final String FIRST_NAME_LOCATOR = "name_f";
    public static final String LAST_NAME_LOCATOR = "name_l";
    public static final String MIDDLE_NAME_LOCATOR = "name_m";

    public static final String DAY_BIRTHDAY_LOCATOR = "bd_d";
    public static final String MONTH_BIRTHDAY_LOCATOR = "bd_m";
    public static final String YEAR_BIRTHDAY_LOCATOR = "bd_y";

    public static final String GENDER_LOCATOR = "gender";

    public static final String COUNTRY_LOCATOR = "cnt";
    public static final String COUNTRY_LOCATOR_DEFAULT_OPTION = "[выбрать]";
    public static final String COUNTRY_LOCATOR_DEFAULT_OPTION_BEL = "Беларусь";
    public static final String REGION_LOCATOR = "reg";
    public static final String CITY_LOCATOR = "city";

    public static final String SUBMIT_LOCATOR = "go";

    public static final String ALERT_MSG_FORM = "msg";
    public static final String ALERT_MSG_LAST_NAME = "Не указана фамилия";
    public static final String ALERT_MSG_FIRST_NAME = "Не указано имя";
    public static final String ALERT_MSG_MIDDLE_NAME = "Не указано отчество";
    public static final String ALERT_MSG_GENDER_NAME = "Не указан пол";
    public static final String ALERT_MSG_PASSWORD = "Неверный пароль";


    public static final String FAMILY_LOCATOR = "family";
    public static final String BIOGRAPHY_LOCATOR = "bio";

    public static List<String> getAlertMessages(){
        List<String> alertMessages = new ArrayList<>();
        alertMessages.add(ALERT_MSG_LAST_NAME);
        alertMessages.add(ALERT_MSG_FIRST_NAME);
        alertMessages.add(ALERT_MSG_MIDDLE_NAME);
        alertMessages.add(ALERT_MSG_GENDER_NAME);
        alertMessages.add(ALERT_MSG_PASSWORD);

        return alertMessages;
    }

    public static List<String> getTextAreaLocators(){
        List<String> textAreas = new ArrayList<>();
        textAreas.add(FAMILY_LOCATOR);
        textAreas.add(BIOGRAPHY_LOCATOR);

        return textAreas;
    }
}
