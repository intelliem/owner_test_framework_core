package com.epam.pages.registration;

import com.epam.browser.ChromeBrowser;
import com.epam.entities.user.model.UserModel;
import com.epam.routes.MainRoutes;
import com.epam.utility.UtilityAlertMessage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class RegistrationPage {

    private UserModel userModel = new UserModel();
    private boolean day, month, year;

    public void openPage(){

        ChromeBrowser.openPage(MainRoutes.REGISTRATION_PAGE);
    }

    public boolean isAt() {

        return ChromeBrowser.getPageTitle().equals(RegistrationPageModel.PAGE_TITLE);
    }

    public boolean isEmptyUserNameFields() {

        List<String> fieldNameLocators = new ArrayList<>();
        fieldNameLocators.add(RegistrationPageModel.FIRST_NAME_LOCATOR);
        fieldNameLocators.add(RegistrationPageModel.LAST_NAME_LOCATOR);
        fieldNameLocators.add(RegistrationPageModel.MIDDLE_NAME_LOCATOR);

        List<WebElement> webElements = ChromeBrowser.findInputFieldsByNameLocator(fieldNameLocators);

        for (WebElement webElement: webElements) {
            if (webElement.getText().isEmpty()) {
                return true;
            }
        }
        return false;
    }

    public boolean isDefaultValueOfUserBirthday() {

        List<String> fieldBirthDayLocators = new ArrayList<>();
        fieldBirthDayLocators.add(RegistrationPageModel.DAY_BIRTHDAY_LOCATOR);
        fieldBirthDayLocators.add(RegistrationPageModel.MONTH_BIRTHDAY_LOCATOR);
        fieldBirthDayLocators.add(RegistrationPageModel.YEAR_BIRTHDAY_LOCATOR);

        List<WebElement> webElements = ChromeBrowser.findInputFieldsByNameLocator(fieldBirthDayLocators);

        for (WebElement webElement : webElements) {
            if (webElement.getAttribute("name").equals(RegistrationPageModel.DAY_BIRTHDAY_LOCATOR)) {
                System.out.println(webElement.getAttribute("value"));
                day = webElement.getAttribute("value").equals(userModel.getDayBirthday());
            }
            else if (webElement.getAttribute("name").equals(RegistrationPageModel.MONTH_BIRTHDAY_LOCATOR)) {
                System.out.println(webElement.getAttribute("value"));
                month = webElement.getAttribute("value").equals(userModel.getMonthBirthday());
            }
            else if (webElement.getAttribute("name").equals(RegistrationPageModel.YEAR_BIRTHDAY_LOCATOR)){
                System.out.println(webElement.getAttribute("value"));
                year = webElement.getAttribute("value").equals(userModel.getYearBirthday());
            }
        }

        if (day && month && year){
            return true;
        }
        return false;
    }

    public boolean isDefaultSelectGenderFields(){

        List<WebElement> webElements = ChromeBrowser.findInputFieldsByNameLocator(RegistrationPageModel.GENDER_LOCATOR);

        for (WebElement webElement : webElements) {
            if (webElement.isSelected()){
                System.out.println("Selected: " + webElement.getAttribute("value"));
                return true;
            }
            System.out.println(webElement.getAttribute("value"));
        }
        return false;
    }

    public boolean isDefaultSelectCountryValue() {

        WebElement country = ChromeBrowser.findInputFieldByNameLocator(RegistrationPageModel.COUNTRY_LOCATOR);
        WebElement region = ChromeBrowser.findInputFieldByNameLocator(RegistrationPageModel.REGION_LOCATOR);
        WebElement city = ChromeBrowser.findInputFieldByNameLocator(RegistrationPageModel.CITY_LOCATOR);

        Select countrySelect = new Select(country);
        countrySelect.selectByVisibleText(RegistrationPageModel.COUNTRY_LOCATOR_DEFAULT_OPTION);

        if (countrySelect.getFirstSelectedOption().getText().equals(RegistrationPageModel.COUNTRY_LOCATOR_DEFAULT_OPTION)){

            System.out.println("Region: " + region.isEnabled());
            System.out.println("City: " + city.isEnabled());

            return region.isEnabled() && city.isEnabled();
        }
        return false;
    }

    public boolean changeSelectCountryValue() {

        WebElement country = ChromeBrowser.findInputFieldByNameLocator(RegistrationPageModel.COUNTRY_LOCATOR);
        WebElement region = ChromeBrowser.findInputFieldByNameLocator(RegistrationPageModel.REGION_LOCATOR);

        Select countrySelect = new Select(country);
        countrySelect.selectByVisibleText(RegistrationPageModel.COUNTRY_LOCATOR_DEFAULT_OPTION_BEL);

        if (countrySelect.getFirstSelectedOption().getText().equals(RegistrationPageModel.COUNTRY_LOCATOR_DEFAULT_OPTION_BEL)){
            System.out.println("Region: " + region.isEnabled());

            return region.isEnabled();
        }
        return false;
    }

    public boolean alertAfterDefaultFormSubmit(){

        List<WebElement> defaultWebElementss = ChromeBrowser.findInputFieldByXPath("//*");
        WebElement submitButton = ChromeBrowser.findInputFieldByNameLocator(RegistrationPageModel.SUBMIT_LOCATOR);

        if (submitButton.getAttribute("type").equals("submit")){
            submitButton.click();
            WebElement alertArea = ChromeBrowser.findInputFieldByIdLocator(RegistrationPageModel.ALERT_MSG_FORM);

            List<WebElement> alertMessages = alertArea.findElements(By.cssSelector("b"));
            List<String> defaultMessages = RegistrationPageModel.getAlertMessages();

            UtilityAlertMessage.getAlertMessages(alertMessages);
            UtilityAlertMessage.checkDefaultMessage(defaultMessages);

            if(UtilityAlertMessage.flagMessages.stream().anyMatch(m -> m == false)){
                return false;
            }

        }
        return true;
    }

    public boolean isTextAreaFields(){

        List<WebElement> webElements = ChromeBrowser.findInputFieldsByNameLocator(RegistrationPageModel.getTextAreaLocators());

        for (WebElement webElement : webElements) {
            if(webElement.getTagName().equals("textarea")){
                return true;
            }
            else return false;
        }
        return false;
    }

}
