package com.epam.factory;

import com.epam.pages.registration.RegistrationPage;

public class PagesFactory {

    public static RegistrationPage getRegistrationPage(){

        return new RegistrationPage();
    }
}
