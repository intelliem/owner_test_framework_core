package com.epam.utility;

import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class UtilityAlertMessage {

    public static String[] messages = null;
    public static List<Boolean> flagMessages = new ArrayList<>();

    public static String[] getAlertMessages(List<WebElement> alertMessages){

        for (WebElement element : alertMessages) {
            messages = element.getText().split(Pattern.quote("\n"));
            System.out.println(messages);
        }
        return messages;
    }

    public static List<Boolean> checkDefaultMessage(List<String> defaultMessages){

        int i = 0;
        for (String alert : defaultMessages) {
            boolean flag = Arrays.stream(messages).anyMatch(message -> message.equals(alert));
            flagMessages.add(flag);
        }
        return flagMessages;
    }

}
