package com.epam.entities.user.model;

public class UserModel {

    private String firstName;
    private String lastName;
    private String middleName;

    private String dayBirthday = "01";
    private String monthBirthday = "10";
    private String yearBirthday = "1970";

    private String login;
    private String password;

    private boolean maritalStatus;
    private String biography;

    public GenderModel getGender() {
        return gender;
    }

    public void setGender(GenderModel gender) {
        this.gender = gender;
    }

    private GenderModel gender;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getDayBirthday() {
        return dayBirthday;
    }

    public void setDayBirthday(String dayBirthday) {
        this.dayBirthday = dayBirthday;
    }

    public String getMonthBirthday() {
        return monthBirthday;
    }

    public void setMonthBirthday(String monthBirthday) {
        this.monthBirthday = monthBirthday;
    }

    public String getYearBirthday() {
        return yearBirthday;
    }

    public void setYearBirthday(String yearBirthday) {
        this.yearBirthday = yearBirthday;
    }

    public GenderModel getSexCategory() {
        return gender;
    }

    public void setSexCategory(GenderModel gender) {
        this.gender = gender;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(boolean maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }
}
